$(document).ready(function () {
    $("#fullpage").fullpage({
        anchors: ["index", "tech1", "tech2", "motion1", "motion2", "content", "admini", "signup"],
        sectionsColor: ["#3867B0", "#8CCED3", "#F3C41D", "#527BBC", "#8C98CB", "#F9D3CB", "#A7BAD0", "#3867B0"],
        // afterLoad: function (anchors, index) {
        //     $('.section:nth-child(' + (index) + ') *').addClass('animated fadeIn');
        // },
        // onLeave: function (index, nextIndex, direction) {
        //     $('.section:nth-child(' + (nextIndex) + ') *').addClass('animated fadeIn');
        //     $('.section:nth-child(' + (index) + ') *').removeClass('animated fadeIn');
        // },
    });
    let height = document.body.clientHeight;
    window.onresize = function () {
        document.body.style.height = height + 'px';
    }
});